/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hello;
import org.springframework.data.annotation.Id;
/**
 *
 * @author DM3-2-20
 */
public class Plant {

	@Id private String id;
        
	private String seedname;
	private String bank;
        private int seedType; //1 Feminizada 2 Regular
        private int specie; //1 Indica 2 Sativa
        private Boolean autoflower;

    public Plant(String id, String seedname, String bank, int seedType, int specie, Boolean autoflower) {
        this.id = id;
        this.seedname = seedname;
        this.bank = bank;
        this.seedType = seedType;
        this.specie = specie;
        this.autoflower = autoflower;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSeedname() {
        return seedname;
    }

    public void setSeedname(String seedname) {
        this.seedname = seedname;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public int getSeedType() {
        return seedType;
    }

    public void setSeedType(int seedType) {
        this.seedType = seedType;
    }

    public int getSpecie() {
        return specie;
    }

    public void setSpecie(int specie) {
        this.specie = specie;
    }

    public Boolean getAutoflower() {
        return autoflower;
    }

    public void setAutoflower(Boolean autoflower) {
        this.autoflower = autoflower;
    }
    
    

}