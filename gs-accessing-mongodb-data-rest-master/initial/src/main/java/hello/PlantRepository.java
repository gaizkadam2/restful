 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hello;

import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
/**
 *
 * @author DM3-2-20
 */
@RepositoryRestResource(collectionResourceRel = "plant", path = "plant")
public interface PlantRepository extends MongoRepository<Plant, String> {

	List<Plant> findBySeedname(@Param("seedname") String seedname);
        List<Plant> findByBank(@Param("bank") String bank);
        List<Plant> findBySeedType(@Param("seedType") String seedType);
        List<Plant> findBySpecie(@Param("specie") String specie);
        List<Plant> findByAutoflower(@Param("autoflower") boolean autoflower);
}
