﻿using Plants.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Net;

namespace Plants.Controllers
{
    public class PlantController : Controller
    {
        //Hosted web API REST Service base url  
        public static string Baseurl = "http://192.168.70.13:8080/";
        public async Task<ActionResult> Index()
        {
            List<Plant> PlantInfo = new List<Plant>();

            Plant plant = new Plant();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync("plant");

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var PlantResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    RootObject ro = JsonConvert.DeserializeObject<RootObject>(PlantResponse);
                    PlantInfo = ro._embedded.plant;

                    try
                    {
                        foreach (Plant planta in ro._embedded.plant)
                        {
                            PlantInfo.Add(planta);

                        }
                    }
                    catch (Exception) {

                    }

                }
                //returning the employee list to view  
                return View(PlantInfo);
            }



        }
        public async Task<ActionResult> NewPlant()
        {
            return View();
        }
        public ActionResult Delete(string id)
        {
            DeleteProductAsync(id);
            return RedirectToAction("Index");
        }
        
        static async Task<HttpStatusCode> DeleteProductAsync(string id)
        {
            using (var client = new HttpClient())
            {
                HttpResponseMessage response = await client.DeleteAsync(
                Baseurl+$"/plant/{id}");
                return response.StatusCode;
            }
        }
        [HttpPost]
        public ActionResult CreateNewPlant(string seedname, string bankname, string option1, string option2, string option3)
        {
           

            Plant plant = new Plant(seedname,bankname,option1,option2,option3);
            CreateProductAsync(plant);
            return RedirectToAction("Index");
        }

       

        static async Task<Uri> CreateProductAsync(Plant plant)
        {
            using (var client = new HttpClient())
            {
                HttpResponseMessage response = await client.PostAsJsonAsync(
                Baseurl+"plant", plant);
                response.EnsureSuccessStatusCode();

                // return URI of the created resource.
                return response.Headers.Location;
            }
        }
        //public HttpResponseMessage PostProduct(Product item)
        //{
        //    item = repository.Add(item);
        //    var response = Request.CreateResponse<Product>(HttpStatusCode.Created, item);

        //    string uri = Url.Link("DefaultApi", new { id = item.Id });
        //    response.Headers.Location = new Uri(uri);
        //    return response;
        //}
        public async Task<ActionResult> EditPlant()
        {
            return View();
        }
    }
}