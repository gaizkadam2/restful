﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Plants.Models
{


    public class Self
    {
        public string href { get; set; }
    }

    public class Plant2
    {
        public string href { get; set; }
    }

    public class Links
    {
        public Self self { get; set; }
        public Plant2 plant { get; set; }
    }

    public class Plant
    {
        public string seedname { get; set; }
        public string bank { get; set; }
        public string seedtype { get; set; }
        public string specie { get; set; }
        public string autoflower { get; set; }
        public Links _links { get; set; }

        public Plant(string seedname, string bank, string seedtype, string specie, string autoflower)
        {
            this.seedname = seedname;
            this.bank = bank;
            this.seedtype = seedtype;
            this.specie = specie;
            this.autoflower = autoflower;
        }

        public Plant()
        {
        }
    }

    public class Embedded
    {
        public List<Plant> plant { get; set; }
    }

    public class Self2
    {
        public string href { get; set; }
        public bool templated { get; set; }
    }

    public class Profile
    {
        public string href { get; set; }
    }

    public class Search
    {
        public string href { get; set; }
    }

    public class Links2
    {
        public Self2 self { get; set; }
        public Profile profile { get; set; }
        public Search search { get; set; }
    }

    public class Page
    {
        public int size { get; set; }
        public int totalElements { get; set; }
        public int totalPages { get; set; }
        public int number { get; set; }
    }

    public class RootObject
    {
        public Embedded _embedded { get; set; }
        public Links2 _links { get; set; }
        public Page page { get; set; }
    }

}